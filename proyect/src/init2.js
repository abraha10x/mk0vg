const config = {
    width: 320 * 2, // Tamanio en pixeles de la scena
    height: 180 * 2, // Tamanio en pixeles de la scena
    parent: "container", // Contenedor padre desde el HTML
    type: Phaser.Auto, // Tipo de renderizacion AUTO,WEBGL, CANVAS
    scene: {
        preload: preload,
        create: create,
        update: update
    }, // JSON de cada una de las funciones
    physics: { // JSON que agrega fisicas
        default: "arcade", // Tipo de fisicas
        arcade: {
            gravity: { // Gravedad de la fisica en un parametro
                //y: 500
            }
        }

    }

}

var game = new Phaser.Game(config) // Inicializacion del objeto Phaser

function preload() { // Funcion preload donde se cargan los items de la scena

    this.load.image("bird", "./assets/bird.png"); // Carga la imagen ("name", "location item")
}

function create() { // Funcion que contiene las funciones aplicables a los items cargados

    this.bird = this.physics.add.image(100, 50, "bird"); // Funcion que agrega un sprite (x, y, "name")

    this.right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D); // Agrega en una variable la tecla que se usara
    this.left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A); // Agrega en una variable la tecla que se usara
    this.down = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S); // Agrega en una variable la tecla que se usara
    this.up = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W); // Agrega en una variable la tecla que se usara

    this.bird.setCollideWorldBounds(true); // agrega colision en los bordes al mover el personaje hacia los bordes
}


function update(time, delta) { // Funcion que actualiza la pantalla en loop infinito, el parametro delta es el tiempo en ms de actualizacion
    if (this.right.isDown) { // Mueve personaje con las teclas
        this.bird.x++;
    } else if (this.left.isDown) {
        this.bird.x--;
    } else if (this.down.isDown) {
        this.bird.y++;
    } else if (this.up.isDown) {
        this.bird.y--;
    }

}