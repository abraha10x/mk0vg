const config = {
    width: 320 * 2, // Tamanio en pixeles de la scena
    height: 180 * 2, // Tamanio en pixeles de la scena
    parent: "container", // Contenedor padre desde el HTML
    type: Phaser.Auto, // Tipo de renderizacion AUTO,WEBGL, CANVAS
    scene: {
        preload: preload,
        create: create,
        update: update
    }, // JSON de cada una de las funciones
    physics: { // JSON que agrega fisicas
        default: "arcade", // Tipo de fisicas
        arcade: {
            gravity: { // Gravedad de la fisica en un parametro
                y: 500
            }
        }

    }

}

var game = new Phaser.Game(config) // Inicializacion del objeto Phaser

function preload() { // Funcion preload donde se cargan los items de la scena

    this.load.image("bird", "./assets/bird.png"); // Carga la imagen ("name", "location item")
}

function create() { // Funcion que contiene las funciones aplicables a los items cargados

    this.bird = this.physics.add.image(80, 100, "bird"); // Funcion que agrega un sprite (x, y, "name")
    this.bird.setScale(2); // Funcion que duplica la escala de tamanio del sprite
    this.bird.flipX = false; // Funcion que gira el sprite en un eje especifico
    this.bird.setOrigin(0.5); // Funcion que setea el origen de un sprite dentro de un mapa
    // Fisicas
    this.bird.setCollideWorldBounds(true); // Funcion que agrega colision en los bordes del mapa
    this.bird.setBounce(0.3); // Funcion que agrega fisicas de rebote
    this.bird.setVelocity(50, 0); // Funcion que agrega velocidad en un parametro ya sea x o y
}

function update(time, delta) { // Funcion que actualiza la pantalla en loop infinito, el parameto delta es el tiempo en ms de actualizacion

}